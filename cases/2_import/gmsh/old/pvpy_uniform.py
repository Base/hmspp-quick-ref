# trace generated using paraview version 5.10.0
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 10

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active source.
a1_uniformvti = GetActiveSource()

# Properties modified on a1_uniformvti
a1_uniformvti.TimeArray = 'None'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
a1_uniformvtiDisplay = Show(a1_uniformvti, renderView1, 'UniformGridRepresentation')

# trace defaults for the display properties.
a1_uniformvtiDisplay.Representation = 'Surface With Edges'
a1_uniformvtiDisplay.ColorArrayName = [None, '']
a1_uniformvtiDisplay.SelectTCoordArray = 'None'
a1_uniformvtiDisplay.SelectNormalArray = 'None'
a1_uniformvtiDisplay.SelectTangentArray = 'None'
a1_uniformvtiDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
a1_uniformvtiDisplay.SelectOrientationVectors = 'None'
a1_uniformvtiDisplay.ScaleFactor = 1.59999999999
a1_uniformvtiDisplay.SelectScaleArray = 'None'
a1_uniformvtiDisplay.GlyphType = 'Arrow'
a1_uniformvtiDisplay.GlyphTableIndexArray = 'None'
a1_uniformvtiDisplay.GaussianRadius = 0.0799999999995
a1_uniformvtiDisplay.SetScaleArray = [None, '']
a1_uniformvtiDisplay.ScaleTransferFunction = 'PiecewiseFunction'
a1_uniformvtiDisplay.OpacityArray = [None, '']
a1_uniformvtiDisplay.OpacityTransferFunction = 'PiecewiseFunction'
a1_uniformvtiDisplay.DataAxesGrid = 'GridAxesRepresentation'
a1_uniformvtiDisplay.PolarAxes = 'PolarAxesRepresentation'
a1_uniformvtiDisplay.ScalarOpacityUnitDistance = 4.239359331780164
a1_uniformvtiDisplay.OpacityArrayName = [None, '']
a1_uniformvtiDisplay.SliceFunction = 'Plane'

# init the 'Plane' selected for 'SliceFunction'
a1_uniformvtiDisplay.SliceFunction.Origin = [7.99999999995, 6.0000000000150004, 0.0]

# reset view to fit data
renderView1.ResetCamera(False)

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [7.99999999995, 6.0000000000150004, 10000.0]
renderView1.CameraFocalPoint = [7.99999999995, 6.0000000000150004, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# change representation type
a1_uniformvtiDisplay.SetRepresentationType('Surface With Edges')

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

# get layout
layout1 = GetLayout()

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(1455, 486)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [7.99999999995, 6.0000000000150004, 10000.0]
renderView1.CameraFocalPoint = [7.99999999995, 6.0000000000150004, 0.0]
renderView1.CameraParallelScale = 9.999999999969

#--------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).