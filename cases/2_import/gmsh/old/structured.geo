Include "/home/lenni/.gmsh/functions.geo";

// this option MUST be specified, otherwise as soon as boundaries are 
// specified as physical curves, cells are no longer written out! 
Mesh.SaveAll=1;

// * * * * * * Parameters * * * * * * //
lc=1;
meshFactor          =1; // to increase resolution
meshWidth           =16;
meshHeight          =12;
meshThickness       =0.3;
baseRes             =meshFactor;


ExX=0;
ExY=0;
ExZ=0; //meshThickness;
ExLayers=0;
mkEx=0;
is2D=1;
useAROnly=1; // aspect ratio instead of progression
noGrading=0;
isSymmetric=0;
isRectangle=1;
useResOnly=1; // use cells per meter for all edges
mkStrc=1; // structured
getEmpties=0;

prog1=2;
prog2=2;
prog3=2;
prog4=2;


X=0;
Y=0;
Z=0;

dX2=meshWidth;
dY3=meshHeight;

res1=baseRes;
res2=baseRes;
res3=baseRes;
res4=baseRes;

Call QuadrangleP;

Coherence Mesh;


Physical Curve("bottom") = {1};
Physical Curve("right" ) = {2};
Physical Curve("top"   ) = {3};
Physical Curve("left"  ) = {4};

//Physical Volume("flow") = {1};

//Physical Surface("domain") = {1};
