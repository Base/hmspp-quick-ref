#!/bin/bash

set -eu

shopt -s expand_aliases
source ~/hmspp/hms/scripts/hms-commands.sh

# script path
sd="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

caseName=""
casePath=""
msh=""
vtkFile=""
vtkExt=""
dummy=""
boundaryFile=""
constantFile=""
sourceFile=""

setUni() {
	caseName="1_uniform"
	vtkExt="vti"
}

setRect() {
	caseName="2_rectangular"
	vtkExt="vtr"
}

setStruct() {
	caseName="3_structured"
	vtkExt="vts"
}

setUnstr() {
	caseName="4_unstructured"
	vtkExt="vtp"
}

setPaths() {
	casePath="${sd}/${caseName}"
	msh="${casePath}/import/${caseName}.msh"
	vtkFile="${casePath}/${caseName}.${vtkExt}"
	boundaryFile="${casePath}/Boundary.xml"
	constantFile="${casePath}/ConstantFields.xml"
	sourceFile="${casePath}/SourceFields.xml"
}

ask() {
	echo "$1? Press return to continue."
	while :; do
		read -r dummy
		[[ -z $dummy ]] && break  # empty input
	done
}

showMsh() {
	ask "Show mesh in Gmsh"
	gmsh "$msh" &
}

import() {
	ask "Import gmsh file into hms"
	hms -m ${casePath}
}

removeFields() {
	$sd/removeSweFields "$vtkFile"
}

showInParaview() {
	ask "Show mesh in ParaView"
	removeFields
	paraview "${vtkFile}" &
}

removeFiles() {
	rm "$vtkFile" "$boundaryFile" "$constantFile" "$sourceFile"
}

setAndRemove() {
	setPaths
	removeFiles
}

removeAllFiles(){
	ask "Remove files"
	setUni
	setAndRemove
	setRect
	setAndRemove
	setStruct
	setAndRemove
	setUnstr
	setAndRemove
}

perMesh() {
	setPaths
	showMsh
	import
	showInParaview
# 	removeFiles
}

uniMesh() {
	echo "Uniform rectangular mesh"
	setUni
	perMesh
}

rectMesh() {
	echo "Non-uniform rectangular mesh"
	setRect
	perMesh
}

structMesh() {
	echo "Non-rectangular structured mesh"
	setStruct
	perMesh
}

unstrMesh() {
	echo "Unstructured mesh"
	setUnstr
	perMesh
}

uniMesh
rectMesh
structMesh
unstrMesh

removeAllFiles
