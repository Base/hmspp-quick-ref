#ifndef HMS_PLUGIN_SLOW_HPP
#define HMS_PLUGIN_SLOW_HPP

#include "plugin.hpp"

namespace hms::plugin
{

HMS_PLUGIN_EXTERN_DECLARE_CREATOR

class Slow : public Plugin
{
public:
	using Plugin::logger;
	virtual bool useHook(Hook) const override;
	virtual void run(Hook) const override;
};

} // namespace hms::plugin


#endif
