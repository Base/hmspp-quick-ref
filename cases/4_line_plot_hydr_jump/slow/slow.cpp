#include "slow.hpp"
#ifdef _WIN32
#include<windows.h>
#else
#include<unistd.h>
#endif 

namespace hms::plugin
{
/* factory function for your plugin */
HMS_PLUGIN_DEFINE_CREATOR(Slow)

/* Every plugin must override this virtual function,
 * and return true for each hook the plugin wants to use.
 * For example, if it wants to use the write time and end hooks,
 * it should return "true" for arg = Hook::writeTime and arg = Hook::end
 * */
bool Slow::useHook(Hook arg) const {
	/* and as an example, it wants to use the write time and end hooks */
	return arg == Hook::writeTime;
}

void Slow::run(Hook) const {
	sleep(1);
}

} // namespace hms::plugin
