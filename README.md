# Quick Reference

Contains a [pdf document](./latex/quickref.pdf) on the installation and usage of hms++, as well as related cases.

It was originally created as a presentation/demo, to which it owes its format,
but it may still be useful as a reference for options, the function parser, boundary conditions, etc.
